package ca.sb.a8.adapter;

import java.util.List;

import org.json.JSONObject;

import com.activeandroid.query.*;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import ca.sb.a8.R.id;
import ca.sb.a8.*;
import ca.sb.a8.model.Student;
public class StudentListAdapter extends ArrayAdapter<Student> {
	private List<Student> list;
	private final Activity context;

	public StudentListAdapter(Activity context, List<Student> list) {
		super(context, R.layout.student_list_element, list);
		this.context = context;
		this.list = list;
	}
	public void setList(List<Student> l)
	{
		list = l;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Student getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	static class ViewHolder {
		public TextView textViewStudentId;
		public TextView textViewStudentFirst;
		public TextView textViewStudentLast;
		public TextView textViewStudentGrade;
		public Button buttonView;
		public Button buttonEdit;
		public Button buttonDelete;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;

		if (convertView == null) {
			LayoutInflater inflator = context.getLayoutInflater();
			convertView = inflator.inflate(R.layout.student_list_element,
					null);

			holder = new ViewHolder();
			holder.textViewStudentId = (TextView) convertView
					.findViewById(R.id.textViewShowStudentListItemID);
			holder.textViewStudentFirst = (TextView) convertView
					.findViewById(R.id.textViewShowStudentListItemFirstName);
			holder.textViewStudentLast = (TextView) convertView
					.findViewById(R.id.textViewShowStudentListItemLastName);
			holder.textViewStudentGrade = (TextView) convertView
					.findViewById(R.id.textViewShowStudentListItemGrade);
			holder.buttonView = (Button) convertView
					.findViewById(R.id.buttonShowStudentListItemView);
			holder.buttonEdit = (Button) convertView
					.findViewById(R.id.buttonShowSingleEdit);
			holder.buttonDelete = (Button) convertView
					.findViewById(id.buttonShowSingleDelete);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		final Student stud = list.get(position);
		holder.textViewStudentId.setTag(stud);
		holder.textViewStudentId.setText("" + list.get(position).getId());
		holder.textViewStudentFirst.setText("" + list.get(position).firstname);
		holder.textViewStudentLast.setText("" + list.get(position).lastname);
		holder.textViewStudentGrade.setText("" + list.get(position).grade);

		holder.buttonView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				RelativeLayout relativeLayout = (RelativeLayout) v.getParent();
				TextView textView = (TextView) relativeLayout.getChildAt(0);
				Student stud = (Student) textView.getTag();
				// Launching new Activity on selecting single List Item
				Intent intent = new Intent(context,
						ViewStudentActivity.class);
				// sending data to new activity
				Bundle bundle = new Bundle();
				bundle.putLong("id", stud.getId());
				intent.putExtras(bundle);
				context.startActivity(intent);
			}
		});

		holder.buttonEdit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				RelativeLayout relativeLayout = (RelativeLayout) v.getParent();
				TextView textView = (TextView) relativeLayout.getChildAt(0);
				Student stud = (Student) textView.getTag();
				// Launching new Activity on selecting single List Item
				Intent intent = new Intent(context, UpdateStudentActivity.class);
				// sending data to new activity
				Bundle bundle = new Bundle();
				bundle.putLong("id", stud.getId());
				intent.putExtras(bundle);
				context.startActivity(intent);
			}
		});

		holder.buttonDelete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				RelativeLayout relativeLayout = (RelativeLayout) v.getParent();
				TextView textView = (TextView) relativeLayout.getChildAt(0);
				Student stud = (Student) textView.getTag();

				// Delete record from remote
				String url = "http://fn-linux.imgd.ca/rest1/students/"
						+ stud.remoteId + ".json";
				final AQuery aq = new AQuery(getContext());
				aq.delete(url, JSONObject.class, this, "jsonDeleteCallback");
				// delete from the list and database
				int pos = list.indexOf(stud);
				if (pos >= 0)
					list.remove(pos);

				if (stud != null)
					new Delete().from(Student.class)
							.where("Id = ?", stud.getId()).executeSingle();

				notifyDataSetChanged();
			}
		});

		return convertView;
	}

	public void jsonDeleteCallback(String url, JSONObject json,
			AjaxStatus status) {
		Toast.makeText(MainActivity.appContext, "Delete Status: "+
			status.getCode(),Toast.LENGTH_SHORT).show();
		// textViewGetResult.setText("JSON - DELETE : " + status.getCode());
	}
}