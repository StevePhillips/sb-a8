package ca.sb.a8;

import org.json.JSONException;
import org.json.JSONObject;

import ca.sb.a8.model.Student;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

public class UpdateStudentActivity extends Activity
{
	EditText editTextFirst;
	EditText editTextLast;
	EditText editTextGrade;
	Student stud;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_update_student);
		// Show the Up button in the action bar.
		setupActionBar();
		// ----------------------------------
		Bundle bndl = this.getIntent().getExtras();
		int studid = (int) bndl.getLong("id");
		stud = new Select().from(Student.class)
				.where("id = ?", studid).executeSingle();
		// ------------------
		editTextFirst = (EditText) findViewById(R.id.editText1);
		editTextLast = (EditText) findViewById(R.id.editText2);
		editTextGrade = (EditText) findViewById(R.id.editText3);
		// ----------------------------------------
		editTextFirst.setText(stud.firstname);
		editTextLast.setText(stud.lastname);
		editTextGrade.setText(""+stud.grade);
		((TextView)findViewById(R.id.textView1)).setText(""+stud.remoteId);
	}
	public void onUpdateClicked(View v)
	{
		String id =    "" + stud.remoteId;
	    String first = editTextFirst.getText().toString();
	    String last =  editTextLast.getText().toString();
	    String grade = editTextGrade.getText().toString();
	    
	    JSONObject input = new JSONObject();
	    try
	    {
	      input.putOpt("id", id);
	      input.putOpt("first_name", first);
	      input.putOpt("last_name", last);
	      input.putOpt("grade", grade);
	    }
	    catch (JSONException exp)
	    {
	          System.out.println("JSON Exception ; " + exp.getMessage());
	    }
	    
	    final AQuery aq = new AQuery(this);
	    String url = "http://fn-linux.imgd.ca/rest1/students/" + id + ".json";
	    aq.put(url, input, JSONObject.class, new AjaxCallback<JSONObject>()
	    {

	      @Override
	      public void callback(String url, JSONObject json, AjaxStatus status)
	      {
	        // SUCCESS - json is null + status says network error (101)
	        if (json != null)
	        {
	          //successful ajax call, show status code and json content
	          Toast.makeText(aq.getContext(),
	                         status.getCode() + ":" + json.toString(),
	                         Toast.LENGTH_LONG).show();
	          System.out.println(json.toString());
	        }
	        else
	        {
	          //ajax error, show error code
	          Toast.makeText(aq.getContext(), "Error:" + status.getCode(),
	                         Toast.LENGTH_LONG).show();
	          System.out.println( "Status: " + status.getCode() + " | " +  status.getMessage() +  " | " + " json = null"); 
	          		
	        }
	      }
	    });
	    finish();
	}
	public void onDeleteClicked(View v)
	{
		String url = "http://fn-linux.imgd.ca/rest1/students/"
				+ stud.remoteId + ".json";
		final AQuery aq = new AQuery(this);
		aq.delete(url, JSONObject.class, this, "jsonDeleteCallback");

		if (stud != null)
			new Delete().from(Student.class)
				.where("Id = ?", stud.getId()).executeSingle();
		finish();
	}
	public void jsonDeleteCallback(String url, JSONObject json,AjaxStatus status)
	{
		Toast.makeText(MainActivity.appContext, "Delete Status: "+
			status.getCode(),Toast.LENGTH_SHORT).show();
		// textViewGetResult.setText("JSON - DELETE : " + status.getCode());
	}
	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.update_student, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
