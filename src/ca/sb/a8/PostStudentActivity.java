package ca.sb.a8;

import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.view.View;
import android.widget.*;


public class PostStudentActivity extends Activity
{
  public static Context appContext;
  EditText editTextPostFirst;
  EditText editTextPostLast;
  EditText editTextPostGrade;
  TextView textViewPostMessage;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_post_student);
    appContext = getApplicationContext();
    
    editTextPostFirst = (EditText)findViewById(R.id.editTextPostFirst);
    editTextPostLast = (EditText)findViewById(R.id.editTextPostLast);
    editTextPostGrade = (EditText)findViewById(R.id.editTextPostGrade);
    textViewPostMessage = (TextView)findViewById(R.id.textViewPostMessage);
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.post_student, menu);
    return true;
  }
  
  public void clickPostCreate(View view)
  {
    String first = editTextPostFirst.getText().toString();
    String last = editTextPostLast.getText().toString();
    String grade = editTextPostGrade.getText().toString();
    
    /*
      params.putString("first_name", stud.getFirst_name());
      params.putString("last_name", stud.getLast_name());
      params.putString("grade", new Integer(stud.getGrade()).toString());
     */
    JSONObject input = new JSONObject();
    try
    {
      input.putOpt("first_name", first);
      input.putOpt("last_name", last);
      input.putOpt("grade", grade);
    }
    catch (JSONException exp)
    {
          textViewPostMessage.setText("JSON Exception ; " + exp.getMessage());
    }
    
    final AQuery aq = new AQuery(this);
    String url = "http://fn-linux.imgd.ca/rest1/students.json";
    aq.post(url, input, JSONObject.class, new AjaxCallback<JSONObject>()
    {

      @Override
      public void callback(String url, JSONObject json, AjaxStatus status)
      {
        if (json != null)
        {
          //successful ajax call, show status code and json content
          Toast.makeText(aq.getContext(),
                         status.getCode() + ":" + json.toString(),
                         Toast.LENGTH_LONG).show();
          //textViewPostMessage.setText(json.toString());
        }
        else
        {
          //ajax error, show error code
          Toast.makeText(aq.getContext(), "Error:" + status.getCode(),
                         Toast.LENGTH_LONG).show();
        }
      }
    });
    finish();
  }
}
