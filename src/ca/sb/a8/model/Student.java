package ca.sb.a8.model;
import com.activeandroid.Model;
// ---------------------
import com.activeandroid.annotation.*;
// --------------------
@Table(name="Student")
public class Student extends Model
{
	@Column(name="First")
	public String firstname;
	@Column(name="Last")
	public String lastname;
	@Column(name="Grade")
	public int grade;
	@Column(name="RemoteId")
	public int remoteId;
	// ------------------
	public Student()
	{
		super();
	}
	// --------------------
	public Student(String first, String last, int Grd, int rId)
	{
		super();
		firstname = first;
		lastname = last;
		grade = Grd;
		remoteId = rId;
	}
}
