package ca.sb.a8;

import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class PutStudentActivity extends Activity
{
  public static Context appContext;
  EditText editTextPutId;
  EditText editTextPutFirst;
  EditText editTextPutLast;
  EditText editTextPutGrade;
  TextView textViewPutMessage;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_put_student);
    appContext = getApplicationContext();
    
    editTextPutId = (EditText)findViewById(R.id.editTextPutId);
    editTextPutFirst = (EditText)findViewById(R.id.editTextPutFirst);
    editTextPutLast = (EditText)findViewById(R.id.editTextPutLast);
    editTextPutGrade = (EditText)findViewById(R.id.editTextPutGrade);
    textViewPutMessage = (TextView)findViewById(R.id.textViewPutMessage);
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.put_student, menu);
    return true;
  }
  
  public void clickPutUpdate(View view)
  {
    String id =    editTextPutId.getText().toString();
    String first = editTextPutFirst.getText().toString();
    String last =  editTextPutLast.getText().toString();
    String grade = editTextPutGrade.getText().toString();
    
    JSONObject input = new JSONObject();
    try
    {
      input.putOpt("id", id);
      input.putOpt("first_name", first);
      input.putOpt("last_name", last);
      input.putOpt("grade", grade);
    }
    catch (JSONException exp)
    {
          textViewPutMessage.setText("JSON Exception ; " + exp.getMessage());
    }
    
    final AQuery aq = new AQuery(this);
    String url = "http://fn-linux.imgd.ca/rest1/students/" + id + ".json";
    aq.put(url, input, JSONObject.class, new AjaxCallback<JSONObject>()
    {

      @Override
      public void callback(String url, JSONObject json, AjaxStatus status)
      {
        // SUCCESS - json is null + status says network error (101)
        if (json != null)
        {
          //successful ajax call, show status code and json content
          Toast.makeText(aq.getContext(),
                         status.getCode() + ":" + json.toString(),
                         Toast.LENGTH_LONG).show();
          textViewPutMessage.setText(json.toString());
        }
        else
        {
          //ajax error, show error code
          Toast.makeText(aq.getContext(), "Error:" + status.getCode(),
                         Toast.LENGTH_LONG).show();
          textViewPutMessage.setText( "Status: " + status.getCode() + " | " +  status.getMessage() +  " | " + " json = null"); 
          		
        }
      }
    });
  }

}
