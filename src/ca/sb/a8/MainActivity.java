package ca.sb.a8;

// --------------------------
import java.util.ArrayList;
import java.util.List;
// -----------------------------------------
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import com.activeandroid.ActiveAndroid;
// ----------------------------------
import com.activeandroid.query.Select;
import com.androidquery.AQuery;
import com.androidquery.util.AQUtility;
import com.androidquery.callback.*;
import ca.sb.a8.adapter.StudentListAdapter;
// ----------------------------------
import ca.sb.a8.model.*;
// --------------------------------------
public class MainActivity extends Activity
{
  public static Context appContext;
  ListView listView;
  List<Student> studentAdapterList;
  StudentListAdapter studentListAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    appContext = getApplicationContext();
    listView = (ListView) findViewById(R.id.listViewStudents);
    // -------------------------------------------------------------------
    studentAdapterList = new Select().all().from(Student.class).execute();
    // ---------------------------------------------------------------
    // If our database is empty start by aquiring the list of students
    // ----------------------------------------
    if (studentAdapterList == null || studentAdapterList.size() == 0)
    {
      studentAdapterList = new ArrayList<Student>();
      String url = "http://fn-linux.imgd.ca/rest1/students.json";
      final AQuery aq = new AQuery(this);
      aq.ajax(url, JSONArray.class, new AjaxCallback<JSONArray>()
      {
        @Override
        public void callback(String url, JSONArray json, AjaxStatus status)
        {
          if (json != null)
          {
            System.out.println("Downloading Student Listing");
            // The documentation states that wrapping save and read operations
            // in begin and end transaction is faster than just raw by a factor of 100
            ActiveAndroid.beginTransaction();
            for (int itr = 0; itr < json.length(); itr++)
            {
              try
              {
                JSONObject obj = json.getJSONObject(itr);
                // ---------------------------------------
                Student stud = new Student(obj.getString("first_name"), obj
                    .getString("last_name"), (int) obj.getLong("grade"),
                    (int) obj.getLong("id"));
                stud.save();
              }
              catch(JSONException e )
              {
                // TODO Auto-generated catch block
                e.printStackTrace();
              }
            }
            ActiveAndroid.setTransactionSuccessful();
            ActiveAndroid.endTransaction();
          }
          else
          {
            // ajax error, show error code
            Toast.makeText(aq.getContext(), "Error:" + status.getCode(),
                           Toast.LENGTH_LONG).show();
          }
        }
      });
    }
    studentAdapterList = new Select().all().from(Student.class).execute();
    studentListAdapter = new StudentListAdapter(this, studentAdapterList);
    listView.setAdapter(studentListAdapter);
    studentListAdapter.notifyDataSetChanged();
  }
  public void onResume()
  {
    super.onResume();
    //studentAdapterList = new Select().all().from(Student.class).execute();
    //studentListAdapter.setList(studentAdapterList);
    studentListAdapter.notifyDataSetChanged();
  }
  public void onFindClicked(View v)
  {
    String query=((EditText)findViewById(R.id.editText1)).getText().toString();
    if(query.compareTo("")==0)
      studentAdapterList = new Select().all().from(Student.class).execute();
    else studentAdapterList =
        new Select("Last").from(Student.class)
          .where("Last = ?",query).execute();
    studentListAdapter.setList(studentAdapterList);
    studentListAdapter.notifyDataSetChanged();
  }
  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }
  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
    case R.id.sortFirst:
      studentAdapterList = new Select().all()
        .from(Student.class).orderBy("First DESC").execute();
      studentListAdapter.setList(studentAdapterList);
      studentListAdapter.notifyDataSetChanged();
      return true;
    case R.id.sortLast:
      studentAdapterList = new Select().all()
        .from(Student.class).orderBy("Last DESC").execute();
      studentListAdapter.setList(studentAdapterList);
      studentListAdapter.notifyDataSetChanged();
      return true;
    case R.id.sortGrade:
      studentAdapterList = new Select().all()
      .from(Student.class).orderBy("Grade DESC").execute();
      studentListAdapter.setList(studentAdapterList);
      studentListAdapter.notifyDataSetChanged();
      return true;
    case R.id.goToRestPost:
      Toast.makeText(appContext, "go to POST", Toast.LENGTH_SHORT).show();
      startActivity(new Intent(this, PostStudentActivity.class));
      return true;
    case R.id.updateListing:
    {
      String url = "http://fn-linux.imgd.ca/rest1/students.json";
      final AQuery aq = new AQuery(this);
      aq.ajax(url, JSONArray.class, new AjaxCallback<JSONArray>()
      {
        @Override
        public void callback(String url, JSONArray json, AjaxStatus status)
        {
          if (json != null)
          {
            ActiveAndroid.beginTransaction();
            for (int itr = 0; itr < json.length(); itr++)
            {
              try {
                JSONObject obj = json.getJSONObject(itr);
                // ---------------------------------------
                Student stud = new Student(obj.getString("first_name"),
                    obj.getString("last_name"), (int) obj.getLong("grade"),
                    (int) obj.getLong("id"));
                stud.save();
              }
              catch(JSONException e) {
                e.printStackTrace();
              }
            }
            ActiveAndroid.setTransactionSuccessful();
            ActiveAndroid.endTransaction();
          }
          else
            Toast.makeText(aq.getContext(), "Error:" + status.getCode(),
                           Toast.LENGTH_LONG).show();
        }
      });
    }
    return true;
    }
    return false;
  }
}
